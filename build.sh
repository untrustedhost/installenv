#!/usr/bin/env bash

# resolve desired imageref and call it ubuntu:latest

[ "${BASE_IMAGE_REFERENCE:-}" ] || BASE_IMAGE_REFERENCE="registry.gitlab.com/untrustedhost/bootable:jammy"

__warn_msg () { echo "${@}" 1>&2 ; }

# get checkout directory git revision information
__get_cr () {
  local cr
  # initial shortrev
  cr="$(git rev-parse --short HEAD)"
  # check for uncommitted files
  { git diff-index --quiet --cached HEAD -- &&
    # check for extra files
    git diff-files --quiet ; } || cr="${cr}-DIRTY"
  echo "${cr}"
}

# derive build stamps here
[ -n "${CODEBASE}" ] || { __warn_msg "CODEBASE not set" ; exit 1 ; }
CODEREV="$(__get_cr)"
TIMESTAMP="$(date +%s)"

case "${CODEREV}" in
  *-DIRTY) __warn_msg "WARNING: git tree is dirty, sleeping 5 seconds for running confirmation."
           sleep 5
           ;;
esac

echo "building ${CODEREV} at ${TIMESTAMP}"

{
  echo "${CODEBASE}_image_coderev=${CODEREV}"
  echo "${CODEBASE}_image_timestamp=${TIMESTAMP}"
} | tee "docker/facts.d/${CODEBASE}.txt"

docker_imagehash=$(docker images -q bootable)
[[ -n "${docker_imagehash}" ]] || {
  docker pull "${BASE_IMAGE_REFERENCE}"
  docker tag  "${BASE_IMAGE_REFERENCE}" 672a32ee-fe85-4d04-80e7-7bb2977c8b3c
}

# build
docker build -t build/release docker
