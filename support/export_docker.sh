#!/usr/bin/env bash

docker run --name=export --entrypoint true registry.gitlab.com/untrustedhost/bootable
docker export export > bootable.tar
pxz bootable.tar
