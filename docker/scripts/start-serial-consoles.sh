#!/usr/bin/env bash

[ -e /proc/tty/driver/serial ] && {
  while read -r i ; do
    [ ! -z "${i}" ] && systemctl start "serial-getty@ttyS${i}"
  done <<< "$(awk -F: '$3 == "16550A port" { print $1 } ; $3 == "ST16650V2 port" { print $1 }' < /proc/tty/driver/serial)"
}

[ -e /proc/tty/driver/usbserial ] && {
  while read -r n ; do
    [ ! -z "${n}" ] && systemctl start "serial-getty@ttyUSB${n}"
  done <<< "$(awk -F': ' '$2 ~ "module" { print $1 }' < /proc/tty/driver/usbserial)"
}

exit 0
