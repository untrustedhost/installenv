#!/usr/bin/env bash

set -eux

[ -d /workdir ] || {
  echo "script requires /workdir volume mount for saving contents" 1>&2
  exit 1
}

# generate iso label based on image facts (codebase/timestamp)
source /usr/lib/untrustedhost/facts.d/installenv.txt
rfs="i${installenv_image_coderev}-${installenv_image_timestamp}"

# also update the hostname in /cdroot/etc here.
echo "${rfs}" > /cdroot/etc/hostname

filtered_cdset=('/cdroot' '/dev' '/sys' '/proc' '/run' '/tmp' '/workdir')
filtered_zisoset=('/cdroot' '/isolinux' '/workdir' '/boot/efiboot.img')

# generate graft points based on the root directory (of ourselves)
graft () {
  local rootfiles=(/*) results=() ent
  for ent in "${rootfiles[@]}" ; do
    case "${filtered_cdset[*]}" in *"${ent}"*) continue ;; esac
    results=("${results[@]}" '-graft-points' "${ent}=${ent}")
  done
  # /cdroot/etc is specifically grafted again after regular etc to replace it.
  results=("${results[@]}" '-graft-points' '/etc=/cdroot/etc')

  # if workdir has a /images or /imd directory, add that in too!
  [ -d /workdir/images ] && results=("${results[@]}" "-graft-points" '/images=/workdir/images')
  [ -d /workdir/imd ] && results=("${results[@]}" "-graft-points" '/IMD=/workdir/IMD')

  echo "${results[@]}"
}

ziso_fset () {
  [ "${DONT_USE_ZISOFS_FILTER:-}" ] && return 0

  local rootfiles=(/*) results=() ent

  # zisofs options
  results=('--zisofs' 'level=9:block_size=128k')

  # zisofs filter
  results=("${results[@]}" 'set_filter_r' '--zisofs')
  for ent in "${rootfiles[@]}" ; do
    case "${filtered_zisoset[*]}" in *"${ent}"*) continue ;; esac
    results=("${results[@]}" "${ent}")
  done

  # finish filter input
  results=("${results[@]}" '--')

  echo "${results[@]}"
}

# calculate graft points, zisofs points
# shellcheck disable=SC2207
{
  graftpts=($(graft))
  zisofs=($(ziso_fset))
}

# temporary image
tempimage=$(mktemp /workdir/.installercore.build.XXXXXX.iso)

#  -eltorito-alt-boot -e /boot/macboot.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
xorriso --report_about HINT -as xorrisofs -U -A "${rfs}" -V "${rfs}" -volset "${rfs}" -r -rational-rock -o "${tempimage}" \
  "${graftpts[@]}" \
  -partition_cyl_align off -partition_offset 0 -apm-block-size 2048 \
  -b isolinux/isolinux.bin -c boot/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table \
  -isohybrid-mbr "/isolinux/isohdpfx.bin" --protective-msdos-label \
  -eltorito-alt-boot -e /boot/efiboot.img -no-emul-boot -isohybrid-gpt-basdat \
  "/cdroot" -- \
  "${zisofs[@]}" \
  -chmodi u+s /usr/bin/sudo --

isohybrid "${tempimage}"

mv "${tempimage}" /workdir/installercore.iso
