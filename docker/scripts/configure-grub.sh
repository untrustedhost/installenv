#!/usr/bin/env bash

set -eux

# load the facter data, set root fs label and boot arguments
source /usr/lib/untrustedhost/facts.d/installenv.txt
rfs="i${installenv_image_coderev}-${installenv_image_timestamp}"
bootargs="root=LABEL=${rfs} ro ${KERNEL_COMMAND_LINE}"

# create a grub config from the contents of /boot
mkdir -p /boot/grub
conf='/boot/grub/grub.cfg'

# preamble
{ echo "set timeout=90" ; } > "${conf}"

# loop
for i in /boot/vmlinuz-* ; do
  # get version mumber
  v="${i%-generic*}" v="${v%-amd64*}" ; v="${v#*vmlinuz-}"
  # get initrd string
  in="${i#*vmlinuz-}"

  # write out label, kernel, initrd, args
  { echo "menuentry ${v} {"
    echo " linux ${i} ${bootargs}"
    [ -e "/boot/initrd.img-${in}" ] && echo " initrd /boot/initrd.img-${in}"
    echo "}"
  } >> "${conf}"
done
