#!/usr/bin/env bash

[ -f "${1}" ] || { echo "provide path to tarball" 1>&2 ; exit 1 ; }

tar xpf "${1}" -C /mnt/sysimage
rsync -av /tmp/overlay/ /mnt/sysimage/

mount -o bind /dev /mnt/sysimage/dev
mount -o bind /proc /mnt/sysimage/proc
mount -o bind /sys /mnt/sysimage/sys

chroot /mnt/sysimage /usr/lib/untrustedhost/scripts/finalize.sh
chroot /mnt/sysimage /usr/lib/untrustedhost/scripts/generate-initrd.sh
chroot /mnt/sysimage /usr/lib/untrustedhost/scripts/install-bootloader.sh
