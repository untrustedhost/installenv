#!/usr/bin/env bash

set -eux

# populate the livecd root, which then gets 'real' root directories mapped into it
mkdir -p /cdroot/var /cdroot/tmp /cdroot/home /cdroot/etc/ssh /cdroot/dev /cdroot/proc /cdroot/sys /cdroot/run /cdroot/root

# fix permissions on cdroot /root directory so sshd doesn't lose the plot
chmod 0700 /cdroot/root

# replace resolv.conf for systemd-resolvd
ln -sf /run/systemd/resolve/resolv.conf /cdroot/etc/resolv.conf

# tmpfiles hackery
ln -sf /dev/null /etc/tmpfiles.d/home.conf
ln -sf ../proc/self/mounts /cdroot/etc/mtab

# things want /var/tmp too, fine
mkdir -p /var/tmp

# pack up directories for r/w ramfs
tar cpf /cdroot/var.tar  -C /var .
xz -T0 /cdroot/var.tar

tar cpf /cdroot/tmp.tar  -C /tmp .
xz -T0 /cdroot/tmp.tar

tar cpf /cdroot/home.tar -C /home .
xz -T0 /cdroot/home.tar

tar cpf /cdroot/root.tar -C /root .
xz -T0 /cdroot/root.tar

# ssh explicily does not pack ssh hostkeys
tar cpf /cdroot/ssh.tar  -C /etc/ssh '--exclude=ssh_host*' .
xz -T0 /cdroot/ssh.tar

rm -rf /var /home /etc/ssh
