#!/usr/bin/env bash

set -eux

# load the facter data, set root fs label and boot arguments
source /usr/lib/untrustedhost/facts.d/installenv.txt
rfs="i${installenv_image_coderev}-${installenv_image_timestamp}"
bootargs="root=LABEL=${rfs} ro ${KERNEL_COMMAND_LINE}"

# create directory, copy files
mkdir /isolinux
cp /usr/lib/ISOLINUX/isolinux.bin /isolinux/isolinux.bin
cp /usr/lib/syslinux/modules/bios/*.c32 /isolinux
cp /usr/lib/ISOLINUX/isohd*.bin /isolinux

# create a syslinux config from the contents of /boot
conf='/isolinux/syslinux.cfg'

# default is used once, so have a state flag
default=""

# preamble
{ echo "timeout 900" ; echo "prompt 1"; } > "${conf}"

# loop
for i in /boot/vmlinuz-* ; do
  # get version mumber
  v="${i%-generic*}" ; v="${v%-amd64}" ; v="${v#*vmlinuz-}"
  # get initrd string
  in="${i#*vmlinuz-}"

  # set the first one as the default
  [ -n "${default}" ] || { default="${v}" ; echo "DEFAULT ${default}" >> "${conf}" ; }

  # write out label, kernel, initrd, args
  { echo "LABEL ${v}"
    echo " KERNEL vmlinuz-${in}"
    [ -e "/boot/initrd.img-${in}" ] && echo " INITRD initrd.img-${in}"
    echo " APPEND ${bootargs}"
  } >> "${conf}"

  # copy files to isolinux dir
  cp "${i}" "/isolinux/${i/boot/}"
  [ -e "/boot/initrd.img-${in}" ] && cp "/boot/initrd.img-${in}" "/isolinux/initrd.img-${in}"
done
